import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void test1() { 
      GraphTask.main (null);
      assertTrue ("There are no tests", true);
   }
   //regular test
   @Test (timeout=1000)
   public void test2(){
      GraphTask gr = new GraphTask();
      GraphTask.Graph g = gr.new Graph("G");
      g.createRandomSimpleGraph(6,9);
      int graph[][] =new int[][] {
              {0, 16, 13, 0, 0, 0},
              {0, 0, 10, 12, 0, 0},
              {0, 4, 0, 0, 14, 0},
              {0, 0, 9, 0, 0, 20},
              {0, 0, 0, 7, 0, 4},
              {0, 0, 0, 0, 0, 0}
      };
      int maxFlow = g.fordFulkerson(graph,4,5);
      assertEquals("Result is incorrect",11,maxFlow);
   }

   //regular test
   @Test (timeout=1000)
   public void test3(){
      GraphTask gr = new GraphTask();
      GraphTask.Graph g = gr.new Graph("G");
      g.createRandomSimpleGraph(6,9);
      int graphTest2[][] = new int[][]{
              {0,5,2,0,0,0},
              {0,0,0,0,0,7},
              {0,8,0,10,0,0},
              {0,8,0,0,0,10},
              {10,0,8,0,0,0},
              {0,0,0,0,0,0}
      };
      int maxFlow = g.fordFulkerson(graphTest2,4,5);
      assertEquals("Result is incorrect",15,maxFlow);
   }

   //regular test
   @Test (timeout=1000)
   public void test4(){
      GraphTask gr = new GraphTask();
      GraphTask.Graph g = gr.new Graph("G");
      g.createRandomSimpleGraph(6,9);
      int graphTest3[][] = new int[][]{
              {0,2,8,0,0,0},
              {0,0,6,7,0,0},
              {0,0,0,0,0,10},
              {0,0,0,0,0,10},
              {10,8,0,0,0,0},
              {0,0,0,0,0,0}
      };
      int maxFlow = g.fordFulkerson(graphTest3,4,5);
      assertEquals("Result is incorrect",17,maxFlow);
   }


   //test no connection between nodes for maxFlow
   @Test (timeout=1000)
   public void test5(){
      GraphTask gr = new GraphTask();
      GraphTask.Graph g = gr.new Graph("G");
      g.createRandomSimpleGraph(6,9);
      int graphTest4[][] = new int[][]{
              {0,0,8,0,0,0},
              {0,0,6,7,0,0},
              {0,0,0,0,16,0},
              {0,0,0,0,0,10},
              {3,0,0,0,0,0},
              {0,0,0,0,0,0}
      };
      int maxFlow = g.fordFulkerson(graphTest4,4,5);
      assertEquals("Result is incorrect",0,maxFlow);
   }


   @Test (expected = IllegalArgumentException.class,timeout=1000)
   //test node out of bounds
   public void test6(){
      GraphTask gr = new GraphTask();
      GraphTask.Graph g = gr.new Graph("G");
      g.createRandomSimpleGraph(6,9);
      int graphTest4[][] = new int[][]{
              {0,2,8,0,0,0},
              {0,0,6,7,0,0},
              {0,5,0,0,0,10},
              {0,0,0,0,0,10},
              {10,8,0,0,0,0},
              {0,0,0,0,0,0}
      };
      int maxFlow = g.fordFulkerson(graphTest4,4,6);

   }

}

